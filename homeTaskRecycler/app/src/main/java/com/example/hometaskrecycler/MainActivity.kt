package com.example.hometaskrecycler

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.hometaskrecycler.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    var numbers= mutableListOf<Int>(1,2,3,4,5)
    private lateinit var binding:ActivityMainBinding
    lateinit var adapter: numberAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding= ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        initAdapter()
        addItem()
    }
    fun initAdapter()
    {
        adapter= numberAdapter()
        adapter.fillNumbers(numbers)
        adapter.updateItem=object : updateItem {
            override fun invoke(p1: Int) {
               numbers.removeAt(p1)
               numbers.add(p1, 1)
                adapter.fillNumbers(numbers)
            }
        }
        adapter.delItem=object : delItem {
            override fun invoke(p1: Int) {
                numbers.removeAt(p1)
                adapter.fillNumbers(numbers)
            }

        }
        binding.rvNumbers.adapter=adapter
        binding.rvNumbers.layoutManager= GridLayoutManager(this,1)

    }
    fun addItem()
    {
        binding.btnAdd.setOnClickListener{
            if(binding.etAddNumber.text.toString().isEmpty())
                Toast.makeText(this, "please enter number value", Toast.LENGTH_SHORT).show()
            else
                adapter.addItem(binding.etAddNumber.text.toString().toInt())

        }
    }
}