package com.example.hometaskrecycler


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.hometaskrecycler.databinding.FirstitemBinding
import com.example.hometaskrecycler.databinding.SeconditemBinding


typealias updateItem=(Int)-> Unit
typealias delItem=(Int)-> Unit
class numberAdapter() :RecyclerView.Adapter<RecyclerView.ViewHolder>(){
    private  var numbers= mutableListOf<Int>()
    public lateinit var updateItem:updateItem
    public lateinit var delItem:delItem
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder = if (viewType == FIRSTVIEW)
             firstViewHolder(
                FirstitemBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
        else
            secondViewHolder(
                SeconditemBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
    override fun getItemViewType(position: Int): Int = if(numbers.get(position)%2==0)
            FIRSTVIEW
        else
            SECONDVIEW
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int)
    {
            if (holder.itemViewType == FIRSTVIEW) {
                holder as firstViewHolder
                holder.binding.tvPinkNumber.text = numbers.get(position).toString()
                holder.onbind()
            } else {
                holder as secondViewHolder
                holder.binding.tvNumber.text = numbers.get(position).toString()
                holder.onbind()
            }
        }
    override fun getItemCount(): Int=numbers.size
    inner class firstViewHolder(var binding: FirstitemBinding) :RecyclerView.ViewHolder(binding.root), View.OnClickListener
    {
        fun onbind()
        {
            binding.btnPinkUpdate.setOnClickListener(this)
            binding.btnPinkSave.setOnClickListener(this)
        }

        override fun onClick(p0: View?) {
            if(p0!!.id==binding.btnPinkUpdate.id)
            {
               updateItem.invoke(absoluteAdapterPosition)
            }
            else
            {
                delItem.invoke(absoluteAdapterPosition)
            }
        }
    }
    inner class secondViewHolder(var binding: SeconditemBinding) :RecyclerView.ViewHolder(binding.root), View.OnClickListener
    {
        fun onbind()
        {
            binding.btnSave.setOnClickListener(this)
            binding.btnUpdate.setOnClickListener(this)
        }
        override fun onClick(p0: View?) {
            if(p0!!.id==binding.btnSave.id)
            {

                delItem.invoke(absoluteAdapterPosition)
            }
            else
            {

                updateItem.invoke(absoluteAdapterPosition)
            }
        }

    }
    public fun fillNumbers(numbersList: MutableList<Int>)
    {
        numbers.clear()
        numbers.addAll(numbersList)
        notifyDataSetChanged()
    }
    companion object {
        private const val FIRSTVIEW=1
        private const val SECONDVIEW=2
    }
    public fun addItem(number : Int)
    {
        numbers.add(number)
        notifyItemInserted(numbers.size-1)
    }



}

